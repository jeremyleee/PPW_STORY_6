from django.shortcuts import render
import json
from django.http import JsonResponse
from django.http import HttpResponse

def Jeremy(request):
	return render(request, 'Jeremy.html')
def progress(request):
	return render(request,'progress.html')
def mydiary(request):
	tgl = request.POST.get('tgl', False)
	catatan = request.POST.get('catatan', False)
	json = JsonResponse( {"tgl":tgl,"catatan":catatan}, safe=False )
	return json
def getdiary(request):
	tgl = request.GET.get('tgl', False)
	catatan= request.GET.get('catatan', False)
	json = JsonResponse( {'tgl':tgl,'catatan':catatan}, safe=False )
	return json