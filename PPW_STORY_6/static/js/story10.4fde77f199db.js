$(function () {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    var email_is_available;
    var timer = 0;

    $('#email').keydown(function () {
        clearTimeout(timer);
        timer = setTimeout(checkValidEmailFormat, 1000);
        toggleButton();
    });

    $('#name').keydown(function () {
        clearTimeout(timer);
        timer = setTimeout(toggleButton, 1000);
    });

    $('#password').keydown(function () {
        clearTimeout(timer);
        timer = setTimeout(toggleButton, 1000);
    });

    $('#form').on('submit', function (event) {
        event.preventDefault();
        console.log("Form Submitted!");
        sendFormData();
    });
function sendFormData() {
        $.ajax({
            method: 'POST',
            url: "/Story10/add_email/",
            headers: {
                "X-CSRFToken": csrftoken,
            },
            data: {
                name: $('#name').val(),
                email: $('#field_email').val(),
                password: $('#password').val(),
            },
            success: function (response) {
                if (response.is_success) {
                    $('#name').val('');
                    $('#field_email').val('');
                    $('#password').val('');
                    $('#submit-btn').prop('disabled', true);
                    // $('.errorlist p').replaceWith("<p class='success'>Data successfully saved!</p>");
                    console.log("Successfully add data");
                } else {
                    // $('.errorlist p').replaceWith("<p class='fail'>Error! Data cannot be saved!</p>");
                }
            },
            error: function () {
                alert("Error, cannot save data to database");
            }
        })
    }


 function validateEmail() {
        $.ajax({
            method: 'POST',
            url: "/Story10/validate-email/",
            headers: {
                "X-CSRFToken": csrftoken,
            },
            data: {
                email: $('#field_email').val(),
            },
            success: function (email) {
                if (email.is_exists) {
                    email_is_available = false;
                    // $('.errorlist p').replaceWith("<p class='fail'>This email is already been used, please use another email!</p>");
                } else {
                    email_is_available = true;
                    toggleButton();
                }
            },
            error: function () {
                alert("Error, cannot validate email!")
            }
        })
    }

 function toggleButton() {
        var password = $('#password').val();
        var name = $('#name').val();
        var email = $('#field_email').val();
        if (password.length !== 0 && name.length !== 0 && email_is_available) {
            $('.errorlist p').replaceWith("<p></p>");
            $('#submit-btn').prop('disabled', false);
        } else if (password.length === 0 && name.length === 0) {
            $('#submit-btn').prop('disabled', true);
            $('.errorlist p').replaceWith("<p class='fail'>Name and password cannot be empty</p>");
        } else if (password.length === 0 && email.length === 0) {
            $('#submit-btn').prop('disabled', true);
            $('.errorlist p').replaceWith("<p class='fail'>Email and password cannot be empty</p>");
        } else if (name.length === 0 && email.length) {
            $('#submit-btn').prop('disabled', true);
            $('.errorlist p').replaceWith("<p class='fail'>Name and email cannot be empty</p>");
        } else if (password.length === 0) {
            $('#submit-btn').prop('disabled', true);
            $('.errorlist p').replaceWith("<p class='fail'>Password cannot be empty</p>");
        } else if (name.length === 0) {
            $('#submit-btn').prop('disabled', true);
            $('.errorlist p').replaceWith("<p class='fail'>Name cannot be empty</p>");
        } else if (email.length === 0) {
            $('#submit-btn').prop('disabled', true);
            $('.errorlist p').replaceWith("<p class='fail'>Email cannot be empty</p>");
        } else {
            $('#submit-btn').prop('disabled', true);
            $('.errorlist p').replaceWith("<p class='fail'>Please enter a valid email format!</p>");
        }
    }
});