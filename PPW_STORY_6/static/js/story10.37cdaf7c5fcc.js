$(function () {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    var email_is_available;

    $('#form').on('submit', function (event) {
        event.preventDefault();
        console.log("Form Submitted!");
        sendFormData();
    });
function sendFormData() {
        $.ajax({
            method: 'POST',
            url: "/Story10/add_email/",
            headers: {
                "X-CSRFToken": csrftoken,
            },
            data: {
                user_name: $('#name').val(),
                user_email: $('#field_email').val(),
                user_password: $('#password').val(),
            },

            dataType: 'json',
            success: function (response) {
                    $('#name').val('');
                    $('#field_email').val('');
                    $('#password').val('');
                    $('#config_password').val('');
                    $('#submit-btn').prop('disabled', true);
                    $('.errorlist p').replaceWith("<p class='success'>Data successfully saved!</p>");
                    console.log("Successfully add data");
            },
            error: function () {
                alert("Error, cannot save data to database");
            }
        })
    }


 function validateEmail() {
        $.ajax({
            method: 'GET',
            url: "/Story10/validate-email/",
            headers: {
                "X-CSRFToken": csrftoken,
            },
            data: {
                email: $('#field_email').val(),
            },
            dataType: 'json',
            success: function (email) {
                if (email.is_taken) {
                    email_is_available = false;
                    $('.errorlist p').replaceWith("<p class='fail'>This email is already been used, please use another email!</p>");
                } else {
                    email_is_available = true;
                    $('#submit-btn').prop('disabled', false);
                }
            },
            error: function () {
                alert("Error, cannot validate email!")
            }
        })
    }
});