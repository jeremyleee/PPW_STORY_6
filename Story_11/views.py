from django.shortcuts import render
from django.contrib.auth import authenticate, login

def my_view(request):
	username = request.POST['username']
	password = request.POST['password']
	user = authenticate(request, username=username,
	password=password)
	if user is not None:
# Register in Session [explained in last slide]
# Redirect to a success page.
...
	else:
# Return an 'invalid login' error message.
# Sometimes return HTTP 403