from django import forms

class form_email(forms.Form):
	user_name =forms.CharField(label= "Name",max_length=100,error_messages={"required":"Please enter your name"}, widget=forms.TextInput(attrs={'id': 'name', 'class': 'form-control', 'placeholder': 'Enter your full name'}))
	user_email = forms.EmailField(label= "Email",max_length=100,error_messages={"required":"Please enter a valid email"},widget=forms.TextInput(attrs={'id': 'field_email', 'class': 'form-control', 'placeholder': 'Enter your Email '}))
	user_password = forms.CharField(label= "Password",max_length=100,error_messages={"required":"Please enter your password"},widget=forms.TextInput(attrs={'id': 'password', 'class': 'form-control', 'placeholder': 'Enter your Password'}))
	config_password = forms.CharField(label = "Re-enter Password",max_length=100,widget=forms.TextInput(attrs={'id':'config_password', 'class': 'form-control', 'placeholder': 'Confirm your password'}))

# class login3(forms.Form):
# 	user_email = forms.EmailField(label= "Email",max_length=100,error_messages={"required":"Please enter a valid email"},widget=forms.TextInput(attrs={'id': 'field_email', 'class': 'form-control', 'placeholder': 'Enter your Email '}))
# 	user_password = forms.CharField(label= "Password",max_length=100,error_messages={"required":"Please enter your password"},widget=forms.TextInput(attrs={'id': 'password', 'class': 'form-control', 'placeholder': 'Enter your Password'}))
# 	