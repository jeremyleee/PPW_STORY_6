$(function () {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    var email_is_available;
    var data ={};
    var timer = 0;

    $('#field_email').keydown(function () {
        clearTimeout(timer);
        timer = setTimeout(checkValidEmailFormat);
        CekFieldForm();
           });
     $('#name').keydown(function () {
        clearTimeout(timer);
        timer = setTimeout(CekFieldForm, 1000);
    });

    $('#password').keydown(function () {
        clearTimeout(timer);
        timer = setTimeout(CekFieldForm, 1000);
    });

    $('#config_password').keydown(function () {
        clearTimeout(timer);
        timer = setTimeout(checkpassword);   
    });
    $('#form').on('submit', function (event) {
        event.preventDefault();
        console.log("Form Submitted!");
        sendFormData();
    });


function sendFormData() {
        $.ajax({
            method: 'POST',
            url: "/Story10/add_email/",
            headers: {
                "X-CSRFToken": csrftoken,
            },
            data: {
                user_name: $('#name').val(),
                user_email: $('#field_email').val(),
                user_password: $('#password').val(),
            },

            dataType: 'json',
            success: function (response) {
                    $('#name').val('');
                    $('#field_email').val('');
                    $('#password').val('');
                    $('#config_password').val('');
                    $('#submit-btn').prop('disabled', true);
                    $('.errorlist p').replaceWith("<p class='success'>Data berhasil disimpan!</p>");
                    console.log("data sudah masuk");
            },
            error: function () {
                alert("Error, tidak bisa menyimpan data ke database!");
            }
        })
    }
function checkValidEmailFormat() {
        var reg = /^([a-zA-Z0-9_.+-])+@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var is_valid = reg.test($('#field_email').val());

        if (is_valid) {
            validateEmail();
        } else {
            $('.errorlist p').replaceWith("<p class='fail'>Tolong masukan format email yang valid</p>");
        }
    }


 function validateEmail() {
        $.ajax({
            method: 'POST',
            url: "/Story10/validate_email/",
            headers: {
                "X-CSRFToken": csrftoken,
            },
            data: {
                user_email:$('#field_email').val(),
            },
            
            dataType:'json',
            success: function (user_email) {
                if (user_email.is_taken) {
                    email_is_available = false;
                     console.log("email sudah ada");
                    $('.errorlist p').replaceWith("<p class='fail'>Email tersebut sudah pernah didaftarkan sebelumnya, silakan daftar dengan email lain </p>");
                } else {
                    email_is_available = true;
                    $('.errorlist p').replaceWith("<p class='success'>Email belum terdaftar di database, silahkan lanjutkan</p>");
                    $('#submit-btn').prop('disabled', false);
                }
            },
            error: function () {
                alert("Error, Tidak dapat memvalidasi email!")
            }
        })
    }

function checkpassword() {
  if (document.getElementById('password').value ==
    document.getElementById('config_password').value) {
    $('#submit-btn').prop('disabled', false);
     $('.errorlist p').replaceWith("<p class='success'>Password match </p>");
  } else {
    $('#submit-btn').prop('disabled', true);
    $('.errorlist p').replaceWith("<p class='fail'>Password not match! </p>");
                } 
}
function CekFieldForm() {
        var password = $('#password').val();
        var name = $('#name').val();
        var email = $('#field_email').val();
        if (password.length !== 0 && name.length !== 0 && email_is_available) {
            $('.errorlist p').replaceWith("<p></p>");
            $('#submit-btn').prop('disabled', false);
        } else if (password.length === 0 && name.length === 0) {
            $('#submit-btn').prop('disabled', true);
            $('.errorlist p').replaceWith("<p class='fail'>Name and password tidak boleh kosong!</p>");
        } else if (password.length === 0 && email.length === 0) {
            $('#submit-btn').prop('disabled', true);
            $('.errorlist p').replaceWith("<p class='fail'>Email dan password tidak boleh kosong!</p>");
        } else if (name.length === 0 && email.length) {
            $('#submit-btn').prop('disabled', true);
            $('.errorlist p').replaceWith("<p class='fail'>Name dan email tidak boleh kosong!</p>");
        } else if (password.length === 0) {
            $('#submit-btn').prop('disabled', true);
            $('.errorlist p').replaceWith("<p class='fail'>Password tidak boleh kosong!</p>");
        } else if (name.length === 0) {
            $('#submit-btn').prop('disabled', true);
            $('.errorlist p').replaceWith("<p class='fail'>Name tidak boleh kosong!</p>");
        } else if (email.length === 0) {
            $('#submit-btn').prop('disabled', true);
            $('.errorlist p').replaceWith("<p class='fail'>Email tidak boleh kosong!</p>");
        } else {
            $('#submit-btn').prop('disabled', true);
            $('.errorlist p').replaceWith("<p class='fail'>Tolong masukan format email yang valid!</p>");
        }
    }

});