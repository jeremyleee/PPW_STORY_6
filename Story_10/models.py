from django.db import models

class Daftar(models.Model):
	user_name =models.CharField(max_length=30)
	user_email = models.CharField(max_length=30, primary_key=True, unique=True, default="abcdefg@example.com")
	user_password = models.CharField(max_length=30)
	def __str__(self):
		return self.user_email
	def as_dict(self):
 		return {
 			"user_name": self.user_name,
 			"user_email": self.user_email,
 			"user_password":self.user_password,
            }