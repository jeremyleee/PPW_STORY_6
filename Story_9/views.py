from django.shortcuts import render
from django.http import HttpResponse
import json
import requests
from google.oauth2 import id_token
from google.auth.transport import requests
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.http import JsonResponse


def bukus(request):
	try : 
		search=request.GET["search"]
	except:
		getBooksJson = requests.get('https://www.googleapis.com/books/v1/volumes?q="quilting"')
		jsonParsed = json.dumps(getBooksJson.json())
	return JsonResponse(jsonParsed)

def book(request):
    return render(request, "Dict.html")

def login(request):
    if request.method == 'POST':
        try:
            token = request.POST['id_token']
            id_info = id_token.verify_oauth2_token(token, requests.Request(),'1046639656665-tb5i4auenn5uvfv27naj61vqff4elonn.apps.googleusercontent.com')
            if id_info['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
                raise ValueError('Wrong issuer.')

            user_id = id_info['sub']
            name = id_info['name']
            email = id_info['email']
            request.session['user_id'] = user_id
            request.session['name'] = name
            request.session['email'] = email
            request.session['bukus'] = []

            return JsonResponse({'status': '0', 'url': reverse('book')})
        except ValueError:
            return JsonResponse({'status': '1'})
    return render(request, 'login.html')


def logout(request):
    request.session.flush()
    return HttpResponseRedirect(reverse('login'))
