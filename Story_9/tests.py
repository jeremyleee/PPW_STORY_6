from django.test import TestCase
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import book
from .views import login
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


class Story9UnitTest(TestCase):

    def test_root_url_is_exist(self):
        response = Client().get('/Story9/login')
        self.assertEqual(response.status_code,301)

    def test_landing_page_is_completed(self):
        request = HttpRequest()
        response =book(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Lets Read!', html_response)