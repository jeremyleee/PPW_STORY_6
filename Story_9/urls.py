from django.conf.urls import url, include
from .views import *

urlpatterns = [
    url(r'book/',book,name='book'),
    url(r'bukus/',bukus,name='bukus'),
    url(r'login/', login, name="login"),
   	url(r'logout/', logout, name="logout"),
    url(r'auth/', include('social_django.urls', namespace='social')),
    ]
