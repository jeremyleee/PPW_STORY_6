var clicks = 0;
var ab=0
$(document).on('click', '#search', function(){
    $('tbody').empty();
    var searchisi= $("input").val();

    $.ajax({
      url: "{% url 'bukus' %}"+"?search="+searchisi,
      datatype: 'json',
      success: function(data){
          console.log("test");
          console.log(data);
          var books_object = jQuery.parseJSON(data);
          console.log(books_object);
          renderHTML(books_object);
      }
    });
  
  
  function renderHTML(data){
      console.log("render");
      var books_list = data.items;
            for (var i = 0; i < books_list.length; i++){
              var title = books_list[i].volumeInfo.title;
              var author = books_list[i].volumeInfo.authors[0];
              var published = books_list[i].volumeInfo.publishedDate;
              var temp = title.replace(/'/g, "\\'");
              var table = 
              '<tr class="table-books">'+
              '<td class = "nomor">' + (i+1) + '</td>' +
              '<td class="title">'+title+'</td>'+
              '<td class= "author">'+author+'</td>'+
              '<td class= "published">' + published+'</td>'+
              '<td class= "favourite">' + '<button id = "star" ><i class = "fa fa-star"</i></button>' + '</td>';
              $('tbody').append(table);
            }
    }
   
  
  
  var counter = 0;
  
  $(function(){
    $(document).on('click', '#star', function(){
      if($(this).hasClass('favorited')){
        counter -= 1;
        $(this).removeClass('favorited');
        $(this).css("color","yellow");
      }
  
      else{
        counter +=1;
        $(this).addClass('favorited');
        $(this).css("color", "black");
      }
  
      $(".aku").html(counter);
    });
  });

  var speed = 150;
  var delay = $('h1').text().length * speed + speed;
  typeEffect($('h1'), speed);
  // setTimeout(function(){
  //   $('p').css('display', 'inline-block');
  //   typeEffect($('p'), speed);
  // }, delay);

});

function typeEffect(element, speed) {
  var text = $(element).text();
  $(element).html('');
  
  var i = 0;
  var timer = setInterval(function() {
          if (i < text.length) {
            $(element).append(text.charAt(i));
            i++;
          } else {
            clearInterval(timer);
          }
        }, speed);
}
$(document).ready(function () {
    gapi.load('auth2', function () {
        gapi.auth2.init();
    });
});
function onSignIn(googleUser) {
    var id_token = googleUser.getAuthResponse().id_token;
    sendToken(id_token);
    console.log("Logged In");
}

function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        console.log('User signed out.');
    });
}

function sendToken(token) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    var html;

    $.ajax({
        method: "POST",
        url: "",
        headers: {
            "X-CSRFToken": csrftoken
        },
        data: {id_token: token},
        success: function (result) {
            console.log("Successfully send token");
            if (result.status === "0") {
                html = "<h4 class='success'>Logged In</h4>";
                window.location.replace(result.url);
            } else {
                html = "<h4 class='fail'>Something error, please report</h4>"
            }
            $("h4").replaceWith(html)
        },
        error: function () {
            alert("Something error, please report")
        }
    })
}
function addFavorites(id) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        method: "POST",
        url: "/add/",
        headers: {
            "X-CSRFToken": csrftoken,
        },
        data: {
            id: id,
        },
        success: function (res) {
            var icon = `<button id="${res.id}" onclick='removeFavorites("${res.id}")'><i class="yellow"></i></button>`;
            $("#" + res.id).replaceWith(icon);
            $("#counter").replaceWith(`<span id='counter'>${res.count}</span>`);
        },
        error: function () {
            alert("Error, cannot get data from server")
        }
    });
}

function removeFavorites(id) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        method: "POST",
        url: "/remove/",
        headers: {
            "X-CSRFToken": csrftoken,
        },
        data: {
            id: id,
        },
        success: function (res) {
            var icon = `<button id="${res.id}" onclick='addFavorites("${res.id}")'><i class="def"></i></button>`;
            $('#' + res.id).replaceWith(icon);
            $("#counter").replaceWith(`<span id='counter'>${res.count}</span>`);
        },
        error: function () {
            alert("Error, cannot get data from server")
        }
    });
}



 