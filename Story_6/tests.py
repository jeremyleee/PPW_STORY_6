
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import *
from django.apps import apps
from .apps import Story6Config
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here. class Lab5UnitTest(TestCase):
class Story_6_UnitTest(TestCase):
    def test_Story_6_url_is_exist(self): 			#test url
            response = Client().get('')
            self.assertEqual(response.status_code, 200)
        
    #def test_Story_6_using_index_func(self):	#index func
     #           found = resolve('')
      #          self.assertEqual(found.func, form)

    def test_submit_url_is_exist(self):
        response = Client().get('/aku')
        self.assertEqual(response.status_code,302)

    def test_landing_page_is_completed(self):
        response = Client().get('')
        html_response = response.content.decode('utf8')
        self.assertIn('Hello, Apa Kabar?', html_response)

    def test_apps(self):
        self.assertEqual(Story6Config.name, 'Story_6')
        self.assertEqual(apps.get_app_config('Story_6').name, 'Story_6')

  # Function Test
class Story6FunctionalTest(TestCase):

    def setUp(self):
      chrome_options = Options()
      chrome_options.add_argument('--dns-prefetch-disable')
      chrome_options.add_argument('--no-sandbox')
      chrome_options.add_argument('--headless')
      chrome_options.add_argument('disable-gpu')
      service_log_path = "./chromedriver.log"
      service_args = ['--verbose']
      self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
      self.selenium.implicitly_wait(25) 
      super(Story6FunctionalTest, self).setUp()

    def tearDown(self):
      self.selenium.quit()
      super(Story6FunctionalTest, self).tearDown()
    
    def test_input_todo(self):
        selenium = self.selenium
        selenium.get('http://localhost:8000')
        time.sleep(5)
        description = selenium.find_element_by_id('id_statusah')
        submit = selenium.find_element_by_id('submit')
        description.send_keys('Coba Coba')
        submit.send_keys(Keys.RETURN)
        self.assertIn( "Coba Coba", selenium.page_source)
    
    def test_css_selector_section(self):
      selenium = self.selenium
      selenium.get('http://localhost:8000')
      section = selenium.find_element_by_tag_name('section').value_of_css_property('background-color')
      time.sleep(5)
      self.assertIn('rgba(255, 165, 0, 1)', section)
    
    def test_css_selector_body(self):
      selenium = self.selenium
      selenium.get('http://localhost:8000')
      body = selenium.find_element_by_tag_name('body').value_of_css_property('background-color')
      time.sleep(5)
      self.assertIn('rgba(0, 0, 0, 1)', body)

    def test_title(self):
      selenium = self.selenium
      selenium.get('http://localhost:8000')
      # self.browser.get('http://localhost:8000/Hello/')
      time.sleep(5)
      self.assertIn("Story 6 PPW", selenium.title)
    
    def test_header(self):
      selenium = self.selenium
      selenium.get('http://localhost:8000')
      header = selenium.find_element_by_tag_name('h1').text
      time.sleep(5)
      self.assertIn("Hello, Apa Kabar?", header)